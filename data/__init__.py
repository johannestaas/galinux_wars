#!/usr/bin/env python
import random, os
from math import pi, e

PLANET_PATH = 'data/planets'
PLANETS_PER_SYSTEM = 4
SYSTEMS_PER_GALAXY = 10
RANDOM_NAMES = []

def random_name():
    file_size = os.stat(PLANET_PATH)[6]
    with open(PLANET_PATH) as file:
        while True:
            file.seek((file.tell() + random.randint(0, file_size - 1)) % file_size)
            file.readline()
            name = file.readline().strip()
            if name not in RANDOM_NAMES and name[0] != '#':
                RANDOM_NAMES.append(name)
                break
        return name

def random_planet_distance(planet_num, amp=0.1695, coef=0.6544):
    distance = amp * e**(coef * (planet_num+1))
    return distance
    

def random_orbit():
    return random.random() * 2 * pi

def __init__():
    pass
