#!/usr/bin/env python

newplayer1 = "You're new here! Mind telling me your name?"
newplayer2 = "You entered %s. Are you sure? (y/n)"
newplayer3 = "Then what do you want to be called?"
