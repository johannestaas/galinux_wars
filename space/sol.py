#!/usr/bin/env python

import data
from gravity import Selectable

class Planet(Selectable):
    def __init__(self):
        Selectable.__init__(self)
        self.name = None
        self.owner = None
        self.solar = None
        self.distance = None
        self.orbit = 0.0
        self.resources = {}
    def mine(self):
        pass

class SolarSystem(Selectable):
    def __init__(self):
        self.name = '%s System' % data.random_name()
        self.star = None
        self.planets = []
        self.nodes = []

    def init_planets(self, count=data.PLANETS_PER_SYSTEM):
        self.planets = []
        distance = 0
        for i in xrange(count):
            planet = Planet()
            planet.solar = self
            planet.owner = None
            planet.name = data.random_name()
            planet.distance = data.random_planet_distance(i)
            planet.orbit = data.random_orbit()
            distance = planet.distance
            self.planets.append(planet)

    def info(self):
        print(self.name)
        print('  nodes:')
        for node in self.nodes:
            print('    - %s' % node.name)
        print('  planets:')
        for planet in self.planets:
            print('    - planet %s:' % planet.name)
            print('        orbit:    %s' % planet.orbit)
            print('        distance: %s' % planet.distance)

    def add_node(self, node):
        if node not in self.nodes:
            self.nodes.append(node)
        if self not in node.nodes:
            node.nodes.append(self)

    def owned(self, player):
        return [planet for planet in self.planets if planet.owner == player]
