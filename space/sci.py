
class Tech():
    def __init__(self):
        self.name = None
        self.parents = []
        self.children = []
        self.energy = 0.0
        self.cost = []
        self.weapon = []
        self.shields = []
        self.modules = []
        self.buildings = []

class TechGraph():
    def __init__(self, tech):
        self.heads = []
    def get_available(self, owned):
        avail = []
        check = []
        for head in self.heads:
            check.append(head)
        while(check):
            for tech in check:
                if tech in owned:
                    check.remove(tech)
                    for kid in check.children:
                        check.append(kid)
                else:
                    good = True
                    for parent in tech.parents:
                        if parent not in owned:
                            good = False
                            break
                    if good:
                        avail.append(tech)
                    check.remove(tech)
        return avail

