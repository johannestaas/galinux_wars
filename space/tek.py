
class Ship():
    def __init__(self):
        self.name = None
        self.engine = None
        self.weapon = None
        self.armor = None
        self.module = None

class Weapon():
    def __init__(self):
        self.name = None
        self.damage = None
        self.method = None

class Armor():
    def __init__(self):
        self.name = None
        self.damage = None
        self.method = None

class Engine():
    def __init__(self):
        self.name = None
        self.speed = None
        self.energy = None

class Module():
    def __init__(self):
        self.name = None

