
class Player():
    def __init__(self):
        self.name = None
        self.ai = None
        self.planets = set()
        self.ships = []
        self.techs = []
        self.resources = {
            'energy': 0,
            'plastic': 0,
            'science': 0,
        }
    def take_planet(self, planet):
        planet.owner = self
        self.planets.add(planet)
    def get_robotech(self):
        return self.robotech
    def get_nanotech(self):
        return self.nanotech
    def get_infotech(self):
        return self.infotech

