#!/usr/bin/env python

from random import choice
from space import bio, sol
from dimension import Dimension
import data

class Galaxy():
    def __init__(self, debug=False):
        self.debug = debug
        if self.debug:
            print('Debugging initiated.')
        self.player = bio.Player()
        self.solars = []
        self.bigbang()
        self.view = Dimension()

    def set_name(self, name):
        self.player.name = name

    def run(self):
        solar = choice(self.solars)
        planet = choice(solar.planets)
        self.player.take_planet(planet)
        for i in solar.planets:
            self.player.take_planet(i)
        self.view.set_view(solar)
        self.view.display_galaxy(self.player)

    def bigbang(self, count=data.SYSTEMS_PER_GALAXY):
        for i in xrange(count):
            solar = sol.SolarSystem()
            solar.init_planets()
            self.solars.append(solar)
        for solar in self.solars:
            node = choice(self.solars)
            solar.add_node(node)
            if choice([True, False]):
                node2 = choice(self.solars)
                solar.add_node(node2)
            if False and choice([True, False, False]):
                node3 = choice(self.solars)
                solar.add_node(node3)
        if self.debug:
            for solar in self.solars:
                solar.info()
                print('')
