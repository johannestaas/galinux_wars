#!/usr/bin/env python

import data
import curses
from math import sin, cos

class Dimension():
    def __init__(self):
        self.system = None
        self.width = 100
        self.height = 100
        self.x = -20
        self.y = -10
        self.top = curses.initscr()
        self.top.border(0)
        self.map_win = curses.newwin(20, 40, 0, 0)
        self.info_win = curses.newwin(20, 40, 21, 0)

    def display_galaxy(self, player):
        self.display_system(player)

    def display_system(self, player):
        curses.start_color()
        curses.init_pair(1, curses.COLOR_BLUE, curses.COLOR_BLACK)
        curses.init_pair(2, curses.COLOR_YELLOW, curses.COLOR_BLACK)
        curses.init_pair(4, curses.COLOR_RED, curses.COLOR_BLACK)
        NORMAL = curses.color_pair(1)
        SUN = curses.color_pair(2)
        PLAYER = curses.color_pair(4)
        curses.curs_set(0)
        owned = self.system.owned(player)
        for planet in owned:
            planet.unselect()
        which = 0
        owned[which].select()
        while True:
            self.map_win.erase()
            self.map_win.border(1)
            # Planet Color
            for planet in self.system.planets:
                x, y = self.find_coords(planet)
                ry = y - self.y
                rx = x - self.x
                if rx <= 0 or rx >= 40:
                    continue
                if ry <= 0 or ry >= 20:
                    continue
                if planet.owner == player:
                    if planet.is_selected():
                        self.map_win.attrset(PLAYER)
                    else:
                        self.map_win.attrset(NORMAL)
                    self.map_win.addch(ry, rx, ord('o'), curses.A_BOLD)
                else:
                    self.map_win.attrset(NORMAL)
                    self.map_win.addch(ry, rx, ord('o'), curses.A_NORMAL)
            ry = 0 - self.y
            rx = 0 - self.x
            # Sun Color
            self.map_win.attrset(SUN)
            if ry >= 0 and rx >= 0 and rx < 100 and ry < 100:
                self.map_win.addch(ry, rx, ord('O'), curses.A_BOLD)
            self.info_win.addstr(0, 0, "dsndsnjjsndnkldsnfsd")
            self.map_win.overlay(self.top)
            self.info_win.overlay(self.top)
            self.map_win.refresh()
            self.info_win.refresh()
            #self.info_win.getstr(1, 0)
            char = self.map_win.getch()
            if char == ord('q'):
                break
            if char == curses.KEY_LEFT:
                which = which - 1
                if which < 0:
                    which = len(owned) - 1
            if char == curses.KEY_RIGHT:
                which = which + 1
                if which >= len(owned):
                    which = 0
            for planet in owned:
                planet.unselect()
            owned[which].select()
        curses.endwin()
    def set_view(self, solar):
        self.system = solar
    def find_coords(self, planet):
        rad = planet.orbit
        dis = planet.distance
        x = int(cos(rad) * dis * 4)
        y = int(sin(rad) * dis * 4)
        return x, y
